## Внимание! ##

Перед запуском проекта требуется запустить восстановление **nuget**-пакетов.
После восстановления также требуется запустить отдельно команду

**Update-Package –reinstall EntityFramework.SqlServerCompact**

## Версия .NET - 4.5 ##

## Используемые технологии ##

### Сервер ###

* ASP.NET MVC 4 (последнее обновление - 4.0.4)
* Entity Framework (работа с БД)
* SQL Server Compact (в качестве БД)
* ASP.NET Identity (вместо устаревшего Membership)
* ASP.NET SignalR (для реализация мгновенных сообщений)
* OWIN - работает в паре с ASP.NET MVC (требуется для работы Identity и SignalR)
* ASP.NET Web Optimization (для минимизации CSS и JS)
* PagedList.Mvc (для реализации постраничной разбивки результатов поиска)
* Newtonsoft.Json
* HtmlAgilityPack (используется при проверке текста публикации на отсутствие ошибок разметки и отсутствие скриптов)

### Клиент ###

* jQuery v2
* jQuery Validation
* jQuery Noty (библиотека для отображения кастомных диалоговых окон)
* Bootstrap v3
* Bootstrap Datepicker
* Bootstrap Typeahead (выполняет роль автокомплита)
* Bootstrap Tagsinput (используется для удобного ввода ключевых слов)
* Bootstrap Maxlength (индикатор количества введенных символов)
* Moment.js (небольшая библиотека для работы с датами)
* Mustache.js (шаблонизатор)

**P.S.**

В файле Startup.cs есть закомментированые строки.
Если их разблокировать то при запуске база будет заполнена временными данными.