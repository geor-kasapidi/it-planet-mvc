﻿using System;
using System.Linq;

using MvcApp.Data.Models;

namespace MvcApp.Extensions
{
    public static class PublicationsQueryExtensions
    {
        public static IQueryable<PublicationInfo> FindByText(this IQueryable<PublicationInfo> publications, string searchText)
        {
            var text = (searchText ?? String.Empty).Trim().ToLower();

            if (String.IsNullOrEmpty(text))
            {
                return publications;
            }

            return publications.Where(v => String.Concat(v.Title, v.Description, v.KeyWords).ToLower().Contains(text));
        }

        public static IQueryable<PublicationInfo> SortBy(this IQueryable<PublicationInfo> publications, string sortKey, int? sortDir)
        {
            var key = (sortKey ?? String.Empty).Trim().ToLower();

            var dir = sortDir ?? 0;

            switch (key)
            {
                case "rating":
                    return dir < 0 ? publications.OrderByDescending(v => v.WhoRated.Count()) : publications.OrderBy(v => v.WhoRated.Count());
                case "date":
                    return dir < 0 ? publications.OrderByDescending(v => v.CreationDate) : publications.OrderBy(v => v.CreationDate);
                default:
                    return publications.OrderByDescending(v => v.Id);
            }
        }
    }
}