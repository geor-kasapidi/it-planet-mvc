﻿using System;
using System.Linq;

using MvcApp.Data.Models;
using MvcApp.ViewModels.Publication;

namespace MvcApp.Extensions
{
    public static class PublicationExtensions
    {
        public static PublicationListEntryViewModel ToListEntry(this PublicationInfo publication)
        {
            return new PublicationListEntryViewModel
            {
                Id = publication.Id,
                AuthorId = publication.Author.Id,
                AuthorName = publication.Author.GetUserName(),
                Title = publication.Title,
                Description = publication.Description,
                CommentsCount = publication.Comments.Count(),
                CreationDate = publication.CreationDate.ToString("f")
            };
        }

        public static PublicationViewModel Render(this PublicationInfo publication, UserInfo currentUser)
        {
            return new PublicationViewModel
            {
                Id = publication.Id,
                UserId = publication.Author.Id,
                UserName = publication.Author.GetUserName(),
                IsAuthor = currentUser != null && currentUser.Id == publication.Author.Id,
                CreationDate = publication.CreationDate.ToString("D"),
                Title = publication.Title,
                Description = publication.Description,
                Content = publication.Content,
                KeyWords = publication.KeyWords.Split(','),
                Rating = new PublicationRatingViewModel
                {
                    Rated = currentUser != null && currentUser.Rated.FirstOrDefault(v => v.Id == publication.Id) != null,
                    Count = publication.WhoRated.Count()
                },
                IsFavourite = currentUser != null && currentUser.Favorites.FirstOrDefault(v => v.Id == publication.Id) != null,
                Views = publication.Views
            };
        }

        public static PublicationEditViewModel GetEditableData(this PublicationInfo publication)
        {
            return new PublicationEditViewModel
            {
                Id = publication.Id,
                Title = publication.Title,
                Description = publication.Description,
                Content = publication.Content,
                KeyWords = publication.KeyWords
            };
        }

        public static void SaveEditedData(this PublicationInfo publication, PublicationEditViewModel data)
        {
            publication.Title = (data.Title ?? String.Empty).Trim();

            publication.Description = (data.Description ?? String.Empty).Trim();

            publication.Content = data.Content;

            publication.KeyWords = !String.IsNullOrEmpty(data.KeyWords) ? String.Join(",", data.KeyWords.Split(',').Select(v => v.Trim())) : null;
        }
    }
}