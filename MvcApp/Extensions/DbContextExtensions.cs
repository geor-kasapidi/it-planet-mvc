﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvcApp.Data;
using MvcApp.Data.Models;
using MvcApp.ViewModels.Publication;
using MvcApp.ViewModels.User;

using PagedList;

namespace MvcApp.Extensions
{
    public static class DbContextExtensions
    {
        public static IList<CommentInfo> GetPublicationComments(this AppDbContext dbContext, int publicationId)
        {
            return
                dbContext.Comments.Where(v => v.Publication.Id == publicationId).OrderByDescending(v => v.Id).ToList();
        }

        public static IPagedList<PublicationListEntryViewModel> FindPublicationsByParams(this AppDbContext dbContext,
            PublicationSearchViewModel searchParams, int pageSize = 10)
        {
            var pagedList =
                dbContext.Publications.FindByText(searchParams.SearchText).SortBy(searchParams.SortKey,
                    searchParams.SortDir).ToPagedList(searchParams.Page ?? 1, pageSize);

            return new StaticPagedList<PublicationListEntryViewModel>(pagedList.Select(v => v.ToListEntry()), pagedList);
        }

        public static IList<UserChatMessageViewModel> GetChatHistory(this AppDbContext dbContext, string firstUserId,
            string secondUserId)
        {
            return
                dbContext.ChatsMessages.Where(
                    v =>
                        v.Sender.Id == firstUserId && v.Reciever.Id == secondUserId ||
                        v.Sender.Id == secondUserId && v.Reciever.Id == firstUserId).OrderByDescending(v => v.Id).Select
                    (v =>
                        new UserChatMessageViewModel
                        {
                            AuthorId = v.Sender.Id,
                            AuthorName = v.Sender.Email,
                            Text = v.Text
                        }).ToList();
        }

        public static IList<UserListEntryViewModel> FindUsersByTextQuery(this AppDbContext dbContext, string query)
        {
            var text = (query ?? String.Empty).ToLower().Trim();

            if (String.IsNullOrEmpty(text))
            {
                return
                    dbContext.Users.OrderBy(v => v.Id).Take(10).ToList().Select(
                        v => new UserListEntryViewModel {UserId = v.Id, UserName = v.GetUserName()}).ToList();
            }

            return
                dbContext.Users.Where(v => String.Concat(v.UserName, v.FirstName, v.LastName).ToLower().Contains(text))
                    .OrderBy(v => v.Id).Take(10).ToList().Select(
                        v => new UserListEntryViewModel {UserId = v.Id, UserName = v.GetUserName()}).ToList();
        }
    }
}