﻿using MvcApp.Data.Models;
using MvcApp.Utils;
using MvcApp.ViewModels.Comment;

namespace MvcApp.Extensions
{
    public static class CommentExtensions
    {
        public static CommentViewModel Render(this CommentInfo comment, UserInfo currentUser)
        {
            return new CommentViewModel
            {
                Id = comment.Id,
                AuthorId = comment.Author.Id,
                AuthorName = comment.Author.GetUserName(),
                AuthorPhoto = ImageHelper.PathToImage(comment.Author.Image),
                Text = comment.Text,
                CreationDate = comment.CreationDate.ToString("f"),
                Removable = currentUser != null && comment.Author.Id == currentUser.Id
            };
        }
    }
}