﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

using MvcApp.Data.Models;
using MvcApp.Properties;
using MvcApp.Utils;
using MvcApp.ViewModels.User;

namespace MvcApp.Extensions
{
    public static class UserExtensions
    {
        public static bool Exists(this UserInfo user)
        {
            return user != null && user.EmailConfirmed;
        }

        public static string GetUserName(this UserInfo user)
        {
            return !String.IsNullOrEmpty(user.FirstName) && !String.IsNullOrEmpty(user.FirstName)
                ? String.Format("{0} {1}", user.FirstName, user.LastName)
                : user.UserName;
        }

        public static UserProfileViewModel GetProfileData(this UserInfo user, UserInfo currentUser)
        {
            var dataPairs = new List<UserDataPairViewModel>();

            if (!String.IsNullOrEmpty(user.FirstName))
            {
                dataPairs.Add(new UserDataPairViewModel {Title = Resources.first_name, Value = user.FirstName});
            }

            if (!String.IsNullOrEmpty(user.LastName))
            {
                dataPairs.Add(new UserDataPairViewModel {Title = Resources.last_name, Value = user.LastName});
            }

            dataPairs.Add(new UserDataPairViewModel
            {
                Title = Resources.user_sex,
                Value =
                    user.Sex == UserSex.Male
                        ? Resources.sex_male
                        : user.Sex == UserSex.Female ? Resources.sex_female : Resources.not_specified
            });

            if (user.BirthDate != null)
            {
                dataPairs.Add(new UserDataPairViewModel
                {
                    Title = Resources.birth_date,
                    Value = user.BirthDate.Value.ToString("D")
                });
            }

            dataPairs.Add(new UserDataPairViewModel
            {
                Title = Resources.registration_date,
                Value = user.RegistrationDate.ToString("D")
            });

            if (!String.IsNullOrEmpty(user.Info))
            {
                dataPairs.Add(new UserDataPairViewModel {Title = Resources.user_info, Value = user.Info});
            }

            return new UserProfileViewModel
            {
                Id = user.Id,
                IsOwner = currentUser != null && currentUser.Id == user.Id,
                IsFriend = currentUser != null && currentUser.Friends.FirstOrDefault(v => v.Id == user.Id) != null,
                Email = user.Email,
                Photo = ImageHelper.PathToImage(user.Image),
                Rating = user.Publications.Sum(v => v.WhoRated.Count()),
                PublicationsCount = user.Publications.Count(),
                FavoritesCount = user.Favorites.Count(),
                DataPairs = dataPairs
            };
        }

        public static UserEditViewModel GetEditableData(this UserInfo user)
        {
            return new UserEditViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Sex = user.Sex,
                BirthDate = user.BirthDate != null ? user.BirthDate.Value.ToString("d") : String.Empty,
                Info = user.Info
            };
        }

        public static void SaveEditedData(this UserInfo user, UserEditViewModel data)
        {
            user.FirstName = (data.FirstName ?? String.Empty).Trim();

            user.LastName = (data.LastName ?? String.Empty).Trim();

            user.Sex = data.Sex;

            DateTime birthDate;

            if (DateTime.TryParse(data.BirthDate, out birthDate))
            {
                user.BirthDate = birthDate;
            }
            else
            {
                user.BirthDate = null;
            }

            user.Info = (data.Info ?? String.Empty).Trim();
        }

        public static UserEditPhotoInfo GetEditablePhotoData(this UserInfo user)
        {
            return new UserEditPhotoInfo {Image = ImageHelper.PathToImage(user.Image), HasChanged = 0};
        }

        public static async Task<bool> SaveEditedPhoto(this UserInfo user, UserEditPhotoInfo data, string appPath)
        {
            if (data.HasChanged == 0)
            {
                return false;
            }

            ImageHelper.DeleteImageFromDisk(user.Image, appPath);

            var image = data.IsBase64
                ? ImageHelper.ImageFromBase64(data.Image)
                : await ImageHelper.DownloadImage(data.ImageUrl);

            if (image == null)
            {
                user.Image = null;

                return true;
            }

            var resizedImage = image.Resize(new Size(200, 200));

            var imageName = resizedImage.SaveToImagesFolder(appPath);

            user.Image = imageName;

            return true;
        }

        private static UserListEntryViewModel ToListEntry(this UserInfo user)
        {
            return new UserListEntryViewModel
            {
                UserId = user.Id,
                UserName = user.GetUserName()
            };
        }

        public static UserFriendListViewModel GetFriendList(this UserInfo user)
        {
            return new UserFriendListViewModel
            {
                Friends = user.Friends.ToList().Select(v => v.ToListEntry()),
                Subscribers = user.Subscribers.ToList().Select(v => v.ToListEntry())
            };
        }
    }
}