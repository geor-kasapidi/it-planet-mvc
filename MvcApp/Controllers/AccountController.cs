﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using MvcApp.Data.Models;
using MvcApp.Properties;
using MvcApp.ViewModels;
using MvcApp.ViewModels.Account;

namespace MvcApp.Controllers
{
    public class AccountController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginViewModel viewModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var result = await SignInManager.PasswordSignInAsync(viewModel.Email, viewModel.Password, viewModel.RememberMe, true);

            switch (result)
            {
                case SignInStatus.Success:
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Search", "Publication");
                case SignInStatus.LockedOut:
                    return Message(MessageStatus.Danger, Resources.login_attemts_limit_exceeded);
                default:
                    ModelState.AddModelError(String.Empty, Resources.login_error);
                    return View(viewModel);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountRegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = new UserInfo {UserName = viewModel.Email, Email = viewModel.Email, RegistrationDate = DateTime.Now, Sex = UserSex.NotSpecified};

            var result = await UserManager.CreateAsync(user, viewModel.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }

                return View(viewModel);
            }

            var token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            var callbackUrl = Url.Action("ConfirmEmail", "Account", new {userId = user.Id, token}, Request.Url.Scheme);

            await UserManager.SendEmailAsync(user.Id, Resources.registration_confirmation, String.Format(Resources.registration_completion_mail_text, callbackUrl));

            return Message(MessageStatus.Info, String.Format(Resources.instructions_mail_sent, viewModel.Email));
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string token)
        {
            if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(token))
            {
                return Message(MessageStatus.Danger, Resources.invalid_input_data);
            }

            IdentityResult result;

            try
            {
                result = await UserManager.ConfirmEmailAsync(userId, token);
            }
            catch (Exception)
            {
                return Message(MessageStatus.Danger, Resources.registration_failure);
            }

            if (!result.Succeeded)
            {
                return Message(MessageStatus.Danger, Resources.registration_failure);
            }

            return Message(MessageStatus.Success, String.Format(Resources.registration_completed, Url.Action("Login", "Account")));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(AccountForgotPasswordViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = await UserManager.FindByEmailAsync(viewModel.Email);

            if (user == null || !user.EmailConfirmed)
            {
                ModelState.AddModelError(String.Empty, Resources.user_not_found);

                return View(viewModel);
            }

            var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            var callbackUrl = Url.Action("ResetPassword", "Account", new {token}, Request.Url.Scheme);

            await UserManager.SendEmailAsync(user.Id, Resources.password_reset, String.Format(Resources.password_reset_mail_text, callbackUrl));

            return Message(MessageStatus.Info, String.Format(Resources.instructions_mail_sent, viewModel.Email));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string token)
        {
            if (String.IsNullOrEmpty(token))
            {
                return Message(MessageStatus.Danger, Resources.invalid_input_data);
            }

            return View(new AccountResetPasswordViewModel {Token = token});
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(AccountResetPasswordViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = await UserManager.FindByEmailAsync(viewModel.Email);

            if (user == null)
            {
                ModelState.AddModelError(String.Empty, Resources.user_not_found);

                return View(viewModel);
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, viewModel.Token, viewModel.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }

                return View(viewModel);
            }

            return Message(MessageStatus.Success, String.Format(Resources.password_reset_completed, Url.Action("Login", "Account")));
        }

        [HttpGet]
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(AccountChangePasswordViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), viewModel.OldPassword, viewModel.NewPassword);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }

                return View(viewModel);
            }

            Auth.SignOut();

            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            Auth.SignOut();

            return RedirectToAction("Search", "Publication");
        }
    }
}