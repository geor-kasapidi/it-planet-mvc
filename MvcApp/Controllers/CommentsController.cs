﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using MvcApp.Data.Models;
using MvcApp.Extensions;

namespace MvcApp.Controllers
{
    public class CommentsController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> All(int? id)
        {
            if (!Request.IsAjaxRequest() || id == null)
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var comments = DbContext.GetPublicationComments(id.Value).Select(v => v.Render(user));

            return Json(comments);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Add(int id, string text)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var comment = new CommentInfo {Author = user, Publication = publication, Text = text, CreationDate = DateTime.Now};

            DbContext.Comments.Add(comment);

            await DbContext.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var comment = DbContext.Comments.FirstOrDefault(v => v.Id == id);

            if (comment == null)
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (comment.Author.Id != user.Id) // только автор может удалять свои комментарии
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            DbContext.Comments.Remove(comment);

            await DbContext.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}