﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using MvcApp.Data.Models;
using MvcApp.Extensions;
using MvcApp.Properties;
using MvcApp.Utils;
using MvcApp.ViewModels;
using MvcApp.ViewModels.Publication;

namespace MvcApp.Controllers
{
    public class PublicationController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Search(PublicationSearchViewModel searchParams)
        {
            ViewBag.Publications = DbContext.FindPublicationsByParams(searchParams);

            return View(searchParams);
        }

        private async Task<ActionResult> UserPublicationList(string id,
            Func<UserInfo, IEnumerable<PublicationListEntryViewModel>> func)
        {
            var user = await UserManager.FindByIdAsync(id ?? String.Empty);

            if (!user.Exists())
            {
                return Message(MessageStatus.Danger, Resources.user_not_found);
            }

            UserInfo currentUser = null;

            if (Request.IsAuthenticated)
            {
                currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            }

            return
                View(new PublicationUserListViewModel
                {
                    UserId = user.Id,
                    UserName = user.GetUserName(),
                    IsOwner = currentUser != null && currentUser.Id == user.Id,
                    Publications = func(user)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> UserList(string id)
        {
            return await UserPublicationList(id, user => user.Publications.ToList().Select(v => v.ToListEntry()));
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> UserFavList(string id)
        {
            return await UserPublicationList(id, user => user.Favorites.ToList().Select(v => v.ToListEntry()));
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Show(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Search");
            }

            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return Message(MessageStatus.Danger, Resources.publication_not_found);
            }

            publication.Views++;

            await DbContext.SaveChangesAsync();

            UserInfo currentUser = null;

            if (Request.IsAuthenticated)
            {
                currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            }

            return View(publication.Render(currentUser));
        }

        [HttpGet]
        [Authorize]
        public ActionResult New()
        {
            return View("PublicationEditor", new PublicationEditViewModel {Id = -1});
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Edit(int id)
        {
            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return Message(MessageStatus.Danger, Resources.publication_not_found);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user.Id != publication.Author.Id) // только автор может редактировать публикацию
            {
                return Message(MessageStatus.Danger, Resources.no_publication_editing_permission);
            }

            return View("PublicationEditor", publication.GetEditableData());
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Save(PublicationEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("PublicationEditor", viewModel);
            }

            // проверка на корректность разметки и отсутствие скриптов

            var verificationResult = PublicationHelper.VerifyContent(viewModel.Content);

            if (verificationResult != PublicationVerificationResult.Success)
            {
                ModelState.AddModelError(String.Empty,
                    verificationResult == PublicationVerificationResult.InvalidMarkup
                        ? Resources.invalid_markup
                        : Resources.js_unallowed);

                return View("PublicationEditor", viewModel);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (viewModel.Id > 0) // обновление
            {
                var publication = DbContext.Publications.FirstOrDefault(v => v.Id == viewModel.Id);

                if (publication == null)
                {
                    return Message(MessageStatus.Danger, Resources.publication_not_found);
                }

                publication.SaveEditedData(viewModel);

                await DbContext.SaveChangesAsync();

                return RedirectToAction("Show", new {id = publication.Id});
            }
            else // создание
            {
                var publication = new PublicationInfo {Author = user, CreationDate = DateTime.Now};

                publication.SaveEditedData(viewModel);

                DbContext.Publications.Add(publication);

                await DbContext.SaveChangesAsync();

                return RedirectToAction("Show", new {id = publication.Id});
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return new HttpNotFoundResult();
            }

            if (publication.Author.Id != user.Id) // только автор может удалять свои публикации
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            DbContext.Publications.Remove(publication);

            await DbContext.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> ToggleRating(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return new HttpNotFoundResult();
            }

            if (publication.Author.Id == user.Id) // автор не может лайкать свои публикации
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            var rated = user.Rated.FirstOrDefault(v => v.Id == publication.Id);

            if (rated != null)
            {
                user.Rated.Remove(rated);
            }
            else
            {
                user.Rated.Add(publication);
            }

            await DbContext.SaveChangesAsync();

            return Json(new PublicationRatingViewModel {Rated = rated == null, Count = publication.WhoRated.Count()});
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> ToggleFavouriteState(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var publication = DbContext.Publications.FirstOrDefault(v => v.Id == id);

            if (publication == null)
            {
                return new HttpNotFoundResult();
            }

            if (publication.Author.Id == user.Id) // автор не может добавлять в избранное свои публикации
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            var isFavourite = user.Favorites.FirstOrDefault(v => v.Id == publication.Id);

            if (isFavourite != null)
            {
                user.Favorites.Remove(publication);
            }
            else
            {
                user.Favorites.Add(publication);
            }

            await DbContext.SaveChangesAsync();

            return Json(isFavourite == null);
        }
    }
}