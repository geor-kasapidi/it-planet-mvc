﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using MvcApp.Data.Models;
using MvcApp.Extensions;
using MvcApp.Properties;
using MvcApp.ViewModels;
using MvcApp.ViewModels.User;

namespace MvcApp.Controllers
{
    public class UserController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Search(string query)
        {
            var users = DbContext.FindUsersByTextQuery(query);

            return Json(users);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Info(string id)
        {
            var user = await UserManager.FindByIdAsync(id ?? String.Empty);

            if (!user.Exists())
            {
                return Message(MessageStatus.Danger, Resources.user_not_found);
            }

            UserInfo currentUser = null;

            if (Request.IsAuthenticated)
            {
                currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            }

            return View(user.GetProfileData(currentUser));
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Edit()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            return View(user.GetEditableData());
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            user.SaveEditedData(viewModel);

            await DbContext.SaveChangesAsync();

            return RedirectToAction("Info", new {id = user.Id});
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> EditPhoto()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            return View(user.GetEditablePhotoData());
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPhoto(UserEditPhotoInfo viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            await user.SaveEditedPhoto(viewModel, Request.PhysicalApplicationPath);

            await DbContext.SaveChangesAsync();

            return RedirectToAction("Info", new {id = user.Id});
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Chat(string id)
        {
            var user = await UserManager.FindByIdAsync(id ?? String.Empty);

            if (!user.Exists())
            {
                return Message(MessageStatus.Danger, Resources.user_not_found);
            }

            if (user.Id == User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            ViewBag.UserName = user.Email;

            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> ChatHistory(string id)
        {
            var user = await UserManager.FindByEmailAsync(id ?? String.Empty);

            if (!user.Exists())
            {
                return new HttpNotFoundResult();
            }

            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            return Json(DbContext.GetChatHistory(currentUser.Id, user.Id));
        }
    }
}