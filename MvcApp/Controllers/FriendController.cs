﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using MvcApp.Extensions;

namespace MvcApp.Controllers
{
    [Authorize]
    public class FriendController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> List()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            return View(user.GetFriendList());
        }

        [HttpPost]
        public async Task<ActionResult> ToggleState(string id)
        {
            if (!Request.IsAjaxRequest())
            {
                return new HttpNotFoundResult();
            }

            var userId = id ?? String.Empty;

            var seekingUser = DbContext.Users.FirstOrDefault(v => v.Id == userId);

            if (!seekingUser.Exists())
            {
                return new HttpNotFoundResult();
            }

            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (currentUser.Id == userId) // нельзя добавлять себя в друзья или удалить себя из друзей
            {
                return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
            }

            var friend = currentUser.Friends.FirstOrDefault(v => v.Id == userId);

            if (friend != null)
            {
                currentUser.Friends.Remove(friend);
            }
            else
            {
                currentUser.Friends.Add(seekingUser);
            }

            await DbContext.SaveChangesAsync();

            return Json(friend == null);
        }
    }
}