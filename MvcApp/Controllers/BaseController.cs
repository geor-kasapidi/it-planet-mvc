﻿using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using MvcApp.Data;
using MvcApp.Identity;
using MvcApp.ViewModels;

namespace MvcApp.Controllers
{
    public class BaseController : Controller
    {
        public AppDbContext DbContext
        {
            get { return HttpContext.GetOwinContext().Get<AppDbContext>(); }
        }

        public AppUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); }
        }

        public AppSignInManager SignInManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<AppSignInManager>(); }
        }

        public IAuthenticationManager Auth
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        public ViewResult Message(MessageStatus status, string text)
        {
            return View("Message", new MessageViewModel
            {
                Status = status,
                Text = text
            });
        }
    }
}