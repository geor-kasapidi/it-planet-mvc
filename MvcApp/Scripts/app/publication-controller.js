﻿function PublicationController(publicationId, isFavourite, rated, ratingValue, isAuthenticated, isAuthor) {
    var controller = this;
    window.publicationController = controller;
    controller.publicationId = publicationId;
    controller.updateRating(rated, ratingValue);
    controller.reloadComments();
    if (!isAuthenticated) {
        // незарегистрированному пользователю недоступны действия с комментариями, рейтингом и статусом "избранное"
        return;
    }
    moment.locale("ru");
    $("#comment_text").maxlength({
        alwaysShow: true,
        placement: "left"
    });
    
    $("#send_comment_btn").click(function () {
        controller.sendComment();
    });
    $("#reload_comments_btn").click(function () {
        controller.reloadComments();
    });
    if (isAuthor) {
        return;
    }
    controller.updateFavouriteState(isFavourite, false);
    $("#toggle_favorite_btn").click(function () {
        controller.toggleFavouriteState();
    });
    $("#toggle_rating_btn").click(function () {
        controller.toggleRating();
    });
}
// обновление иконки и текста на кнопке "избранная публикация"
PublicationController.prototype.updateFavouriteState = function (isFavourite, showTooltip) {
    var toggleBtn = $("#toggle_favorite_btn");
    toggleBtn.removeClass("glyphicon-star-empty").removeClass("glyphicon-star").addClass(isFavourite ? "glyphicon-star" : "glyphicon-star-empty");
    if (!toggleBtn.data("tooltip-init")) {
        toggleBtn.tooltip().data("tooltip-init", true);
    }
    toggleBtn.attr("data-original-title", isFavourite ? "В избранном" : "В избранное").tooltip("fixTitle");
    if (showTooltip) {
        toggleBtn.tooltip("show");
    }
};
// отправка запроса на добавление в избранное/удаление из избранного
PublicationController.prototype.toggleFavouriteState = function () {
    var controller = this;
    $.post("/Publication/ToggleFavouriteState", {
        id: controller.publicationId
    }, function (response) {
        controller.updateFavouriteState(response, true);
    });
};
// обновление иконки на кнопке "рейтинг" и обновление значения рейтинга
PublicationController.prototype.updateRating = function (rated, count) {
    $("#toggle_rating_btn").removeClass("glyphicon-thumbs-up").removeClass("glyphicon-thumbs-down").addClass(rated ? "glyphicon-thumbs-down" : "glyphicon-thumbs-up");
    $("#rating_value").text(count);
};
// отправка запроса на лайк/дислайк
PublicationController.prototype.toggleRating = function () {
    var controller = this;
    $.post("/Publication/ToggleRating", {
        id: controller.publicationId
    }, function (response) {
        controller.updateRating(response.Rated, response.Count);
    });
};
// метод рендеринга комментариев (используется движок Mustache)
PublicationController.prototype.renderComments = function (comments) {
    var template = $("#comment_template").html();
    Mustache.parse(template);
    var commentsContainer = $("#comments");
    commentsContainer.empty();
    comments.forEach(function (comment) {
        var rendered = Mustache.render(template, comment);
        commentsContainer.append(rendered);
    });
};
// обновление списка комментариев
PublicationController.prototype.reloadComments = function () {
    var controller = this;
    var reloadButton = $("#reload_comments_btn");
    reloadButton.button("loading");
    $.post("/Comments/All", {
        id: controller.publicationId
    }, function (response) {
        controller.renderComments(response);
    }).always(function () {
        reloadButton.button("reset");
        if (!reloadButton.data("tooltip-init")) {
            reloadButton.tooltip().data("tooltip-init", true);
        }
        reloadButton.attr("data-original-title", moment().calendar()).tooltip("fixTitle");
    });
};
// метод отправки комментариев
PublicationController.prototype.sendComment = function () {
    var controller = this;
    var text = $("#comment_text").val().trim();
    if (!text.length) {
        return;
    }
    $.post("/Comments/Add", {
        id: controller.publicationId,
        text: text
    }, function () {
        $("#comment_text").val("");
        controller.reloadComments();
    });
};
// подтверждение удаления комментария (окно подтверждения рендерится по шаблону)
PublicationController.prototype.confirmCommentDeletion = function (element, id) {
    if (!$(element).data("popover-init")) {
        var template = $("#comment_delete_confirmation_template").html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, { id: id });
        $(element).popover({
            trigger: "focus",
            placement: "left",
            content: rendered,
            html: true
        }).data("popover-init", true);
    }
    $(element).trigger("focus");
};
// удаление комментария
PublicationController.prototype.deleteComment = function (id) {
    $("#comment_" + id).remove();
    $.post("/Comments/Delete", {
        id: parseInt(id)
    });
};