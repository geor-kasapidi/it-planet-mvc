﻿function SignalrController(currentUserName, gotoChatLocaledString, closePushLocalizedString) {
    var chat = $.connection.chatHub;
    window.chat = chat;
    // push уведомления
    chat.displayNotification = function (data) {
        var notification = noty({
            text: data.AuthorName + " - " + data.Text,
            theme: "relax",
            layout: "bottomLeft",
            dismissQueue: true,
            buttons: [
                {
                    addClass: "btn btn-success",
                    text: gotoChatLocaledString,
                    onClick: function ($noty) { // переход к диалогу
                        window.location.href = "/User/Chat/" + $noty.authorId;
                    }
                },
                {
                    addClass: "btn btn-danger",
                    text: closePushLocalizedString,
                    onClick: function ($noty) { // закрыть push уведомление
                        $noty.close();
                    }
                }
            ],
            animation: {
                open: { height: "toggle" },
                close: { height: "toggle" },
                easing: "swing",
                speed: 200
            }
        });
        notification.authorId = data.AuthorId;
    };
    // если чат с этим пользователем неактивен, то отображаем push уведомление, иначе - добавляем сообщение в чат
    // код работает в паре с chat-controller.js (этот скрипт загружается на странице чата)
    chat.client.sendMessage = function (data) {
        if (data.AuthorName !== chat.currentCompanion && data.AuthorName !== currentUserName) {
            chat.displayNotification(data);
        } else if (typeof (chat.renderMessage) === "function") {
            chat.renderMessage(data);
        }
    };
    $.connection.hub.start().done(function () {
        if (typeof (chat.activateChat) === "function") {
            chat.activateChat();
        }
    });
}