﻿function ChatController(userName) {
    $("#send_message_btn").button("loading");
    // сохраняем имя текущего пользователя, для фильтрации отображения сообщений - push или в чат
    window.chat.currentCompanion = userName;
    window.chat.renderMessage = function (message) {
        var template = $("#message_template").html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, message);
        $("#messages").prepend(rendered);
    };
    // когда signalr активирован, включаем кнопку отправки сообщений
    window.chat.activateChat = function () {
        $("#send_message_btn").button("reset").removeAttr("disabled").click(function () {
            var text = $("#message_text").val().trim();
            $("#message_text").val("");
            if (text.length) {
                window.chat.server.send(userName, text);
            }
        });
    };
    var renderHistory = function (messages) {
        var template = $("#message_template").html();
        Mustache.parse(template);
        var messagesContainer = $("#messages");
        messages.forEach(function (message) {
            var rendered = Mustache.render(template, message);
            messagesContainer.append(rendered);
        });
    };
    // загружаем историю чата
    $.post("/User/ChatHistory", {
        id: window.chat.currentCompanion
    }, function (response) {
        renderHistory(response);
    });
}