﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace MvcApp.Utils
{
    public static class ImageHelper
    {
        private const string ImagesDir = "Images";

        private const string StartBase64String = "data:image/jpeg;base64,";

        private static Image ImageFromBytes(byte[] imageBytes)
        {
            return Image.FromStream(new MemoryStream(imageBytes));
        }

        /// <summary>
        ///     Создает изображение из Base64 строки
        /// </summary>
        public static Image ImageFromBase64(string base64String)
        {
            try
            {
                var imageBase64String = base64String.Replace(StartBase64String, String.Empty);

                var imageBytes = Convert.FromBase64String(imageBase64String);

                return ImageFromBytes(imageBytes);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///     Скачивает изображение по заданному адресу, не блокируя вызывающий поток
        /// </summary>
        public static async Task<Image> DownloadImage(string imageUrl)
        {
            return await Task<Image>.Factory.StartNew(() =>
            {
                if (!Uri.IsWellFormedUriString(imageUrl, UriKind.Absolute))
                {
                    return null;
                }

                try
                {
                    var imageBytes = new WebClient().DownloadData(new Uri(imageUrl));

                    return ImageFromBytes(imageBytes);
                }
                catch (Exception)
                {
                    return null;
                }
            });
        }

        /// <summary>
        ///     Изменяет размеры изображения
        /// </summary>
        public static Image Resize(this Image image, Size size)
        {
            if (image == null)
            {
                return null;
            }

            try
            {
                using (var bitmap = new Bitmap(size.Width, size.Height))
                {
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.CompositingMode = CompositingMode.SourceCopy;
                        graphics.CompositingQuality = CompositingQuality.HighSpeed;
                        graphics.SmoothingMode = SmoothingMode.HighSpeed;
                        graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                        graphics.InterpolationMode = InterpolationMode.Low;
                        graphics.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height);
                    }

                    var stream = new MemoryStream();

                    bitmap.Save(stream, ImageFormat.Jpeg);

                    return Image.FromStream(stream);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///     Возращает относительный путь до изображения на сервере
        /// </summary>
        public static string PathToImage(string imageName)
        {
            return !String.IsNullOrEmpty(imageName) ? String.Format("/{0}/{1}", ImagesDir, imageName) : null;
        }

        /// <summary>
        ///     Сохраняет изображение в папке на сервере
        /// </summary>
        /// <returns>Возвращает имя файла (GUID)</returns>
        public static string SaveToImagesFolder(this Image image, string appPath)
        {
            if (image == null)
            {
                return null;
            }

            try
            {
                var imageName = String.Format("{0}.jpg", Guid.NewGuid());

                var absolutePath = Path.Combine(appPath, ImagesDir, imageName);

                image.Save(absolutePath);

                return imageName;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///     Удаляет изображение из файловой системы
        /// </summary>
        public static void DeleteImageFromDisk(string imageName, string appPath)
        {
            if (String.IsNullOrEmpty(imageName))
            {
                return;
            }

            try
            {
                var absolutePath = Path.Combine(appPath, ImagesDir, imageName);

                if (File.Exists(absolutePath))
                {
                    File.Delete(absolutePath);
                }
            }
            catch (Exception) {}
        }
    }
}