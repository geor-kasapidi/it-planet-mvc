﻿using System;
using System.Linq;

using HtmlAgilityPack;

namespace MvcApp.Utils
{
    public enum PublicationVerificationResult
    {
        InvalidMarkup,

        ContainsJavaScript,

        Success
    }

    public static class PublicationHelper
    {
        public static PublicationVerificationResult VerifyContent(string html)
        {
            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(String.Format("<div>{0}</div>", html));

            if (htmlDoc.ParseErrors.Any())
            {
                return PublicationVerificationResult.InvalidMarkup;
            }

            if (htmlDoc.DocumentNode.SelectSingleNode(".//script") != null)
            {
                return PublicationVerificationResult.ContainsJavaScript;
            }

            return PublicationVerificationResult.Success;
        }
    }
}