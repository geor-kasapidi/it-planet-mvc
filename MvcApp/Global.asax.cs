﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using MvcApp.Frontend;

namespace MvcApp
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            RouteTable.Routes.MapRoute("Default", "{controller}/{action}/{id}", new
            {
                controller = "Publication",
                action = "Search",
                id = UrlParameter.Optional
            });

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
            BundleTable.Bundles.RegisterAssets();
        }
    }
}