﻿using System.Data.Entity;

using MvcApp.Data;
using MvcApp.Identity;

using Owin;

namespace MvcApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
#if DEBUG

    // РАЗБЛОКИРОВАТЬ ЭТОТ КОД ДЛЯ ЗАПОЛНЕНИЯ БД ВРЕМЕННЫМИ ДАННЫМИ !!!
    // *******

    //Database.SetInitializer(new RandomDataGenerator());

    //using (var dbContext = new AppDbContext())
    //{
    //    dbContext.Database.Initialize(false);
    //}

    // *******
#else
            Database.SetInitializer(new CreateDatabaseIfNotExists<AppDbContext>());
#endif

            // автоматическое создание подключения к БД во время запроса

            app.CreatePerOwinContext(AppDbContext.Create);

            // подключение пользовательской системы

            app.MapIdentity();

            // активация realtime сообщения между клиентами и сервером

            app.MapSignalR();
        }
    }
}