﻿namespace MvcApp.ViewModels.Publication
{
    public class PublicationRatingViewModel
    {
        public bool Rated { get; set; }

        public int Count { get; set; }
    }
}