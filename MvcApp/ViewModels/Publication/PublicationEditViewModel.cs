﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using MvcApp.Properties;

namespace MvcApp.ViewModels.Publication
{
    public class PublicationEditViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(ResourceType = typeof (Resources), Name = "publication_title")]
        public string Title { get; set; }

        [Required]
        [MaxLength(200)]
        [Display(ResourceType = typeof (Resources), Name = "publication_description")]
        public string Description { get; set; }

        [Required]
        [AllowHtml]
        [Display(ResourceType = typeof (Resources), Name = "publication_text")]
        public string Content { get; set; }

        [Required]
        [Display(ResourceType = typeof (Resources), Name = "publication_keywords")]
        public string KeyWords { get; set; }
    }
}