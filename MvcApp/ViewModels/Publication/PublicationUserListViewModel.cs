﻿using System.Collections.Generic;

namespace MvcApp.ViewModels.Publication
{
    public class PublicationUserListViewModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public bool IsOwner { get; set; }

        public IEnumerable<PublicationListEntryViewModel> Publications { get; set; }
    }
}