﻿namespace MvcApp.ViewModels.Publication
{
    public class PublicationListEntryViewModel
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int CommentsCount { get; set; }

        public string CreationDate { get; set; }
    }
}