﻿namespace MvcApp.ViewModels.Publication
{
    public class PublicationViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public bool IsAuthor { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public string[] KeyWords { get; set; }

        public string CreationDate { get; set; }

        public PublicationRatingViewModel Rating { get; set; }

        public bool IsFavourite { get; set; }

        public int Views { get; set; }
    }
}