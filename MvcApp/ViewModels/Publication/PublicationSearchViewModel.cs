﻿namespace MvcApp.ViewModels.Publication
{
    public class PublicationSearchViewModel
    {
        public string SearchText { get; set; }

        public string SortKey { get; set; }

        public int? SortDir { get; set; }

        public int? Page { get; set; }
    }
}