﻿using System.Collections.Generic;

namespace MvcApp.ViewModels.User
{
    public class UserProfileViewModel
    {
        public IEnumerable<UserDataPairViewModel> DataPairs;

        public string Id { get; set; }

        public bool IsOwner { get; set; }

        public bool IsFriend { get; set; }

        public string Email { get; set; }

        public string Photo { get; set; }

        public int Rating { get; set; }

        public int PublicationsCount { get; set; }

        public int FavoritesCount { get; set; }
    }
}