﻿namespace MvcApp.ViewModels.User
{
    public class UserListEntryViewModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}