﻿namespace MvcApp.ViewModels.User
{
    public class UserDataPairViewModel
    {
        public string Title { get; set; }

        public string Value { get; set; }
    }
}