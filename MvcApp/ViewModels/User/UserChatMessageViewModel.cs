﻿namespace MvcApp.ViewModels.User
{
    public class UserChatMessageViewModel
    {
        public string Text { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }
    }
}