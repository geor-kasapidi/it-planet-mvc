﻿using System.Collections.Generic;

namespace MvcApp.ViewModels.User
{
    public class UserFriendListViewModel
    {
        public IEnumerable<UserListEntryViewModel> Friends { get; set; }

        public IEnumerable<UserListEntryViewModel> Subscribers { get; set; }
    }
}