﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Data.Models;
using MvcApp.Properties;

namespace MvcApp.ViewModels.User
{
    public class UserEditViewModel
    {
        [RegularExpression("([a-zA-Zа-яА-Я0-9 ]+)", ErrorMessageResourceType = typeof (Resources), ErrorMessageResourceName = "field_contains_invalid_characters")]
        [Display(ResourceType = typeof (Resources), Name = "first_name")]
        public string FirstName { get; set; }

        [RegularExpression("([a-zA-Zа-яА-Я0-9 ]+)", ErrorMessageResourceType = typeof (Resources), ErrorMessageResourceName = "field_contains_invalid_characters")]
        [Display(ResourceType = typeof (Resources), Name = "last_name")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [Display(ResourceType = typeof (Resources), Name = "birth_date")]
        public string BirthDate { get; set; }

        [Display(ResourceType = typeof (Resources), Name = "user_sex")]
        public UserSex Sex { get; set; }

        [Display(ResourceType = typeof (Resources), Name = "user_info")]
        public string Info { get; set; }
    }
}