﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Properties;

namespace MvcApp.ViewModels.User
{
    public class UserEditPhotoInfo
    {
        [Url]
        [Display(ResourceType = typeof (Resources), Name = "image_url")]
        public string ImageUrl { get; set; }

        public string Image { get; set; }

        public bool IsBase64 { get; set; }

        public int HasChanged { get; set; }
    }
}