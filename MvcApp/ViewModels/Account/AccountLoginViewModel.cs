﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Properties;

namespace MvcApp.ViewModels.Account
{
    public class AccountLoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof (Resources), Name = "email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Resources), Name = "password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof (Resources), Name = "remember_me")]
        public bool RememberMe { get; set; }
    }
}