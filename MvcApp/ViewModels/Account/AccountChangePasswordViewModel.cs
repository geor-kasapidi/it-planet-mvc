﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Properties;

namespace MvcApp.ViewModels.Account
{
    public class AccountChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Resources), Name = "current_password")]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Resources), Name = "new_password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessageResourceType = typeof (Resources), ErrorMessageResourceName = "passwords_do_not_match")]
        [Display(ResourceType = typeof (Resources), Name = "confirm_password")]
        public string ConfirmNewPassword { get; set; }
    }
}