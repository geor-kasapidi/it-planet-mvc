﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Properties;

namespace MvcApp.ViewModels.Account
{
    public class AccountForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof (Resources), Name = "email")]
        public string Email { get; set; }
    }
}