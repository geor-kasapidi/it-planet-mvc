﻿using System.ComponentModel.DataAnnotations;

using MvcApp.Properties;

namespace MvcApp.ViewModels.Account
{
    public class AccountResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof (Resources), Name = "email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Resources), Name = "new_password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof (Resources), ErrorMessageResourceName = "passwords_do_not_match")]
        [Display(ResourceType = typeof (Resources), Name = "confirm_password")]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }
}