﻿namespace MvcApp.ViewModels.Comment
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public string CreationDate { get; set; }

        public bool Removable { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorPhoto { get; set; }
    }
}