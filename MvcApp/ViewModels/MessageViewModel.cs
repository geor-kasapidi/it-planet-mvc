﻿namespace MvcApp.ViewModels
{
    public enum MessageStatus
    {
        Success,

        Info,

        Warning,

        Danger
    }

    public class MessageViewModel
    {
        public MessageStatus Status { get; set; }

        public string Text { get; set; }
    }
}