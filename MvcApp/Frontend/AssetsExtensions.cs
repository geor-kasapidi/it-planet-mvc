﻿using System.Web.Optimization;

namespace MvcApp.Frontend
{
    public static class AssetsExtensions
    {
        public static void RegisterAssets(this BundleCollection bundles)
        {
            // сборка скриптов

            var jsLibs = new[]
            {
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.signalR-2.2.0.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap3-typeahead.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/locales/bootstrap-datepicker.ru.min.js",
                "~/Scripts/bootstrap-tagsinput.js",
                "~/Scripts/bootstrap-maxlength.js",
                "~/Scripts/mustache.js",
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/jquery.noty.packaged.js"
            };

            bundles.Add(new ScriptBundle("~/appjs").Include(jsLibs));

            // добавление рукописных скриптов

            bundles.Add(new ScriptBundle("~/signalrjs").Include("~/Scripts/app/signalr-controller.js"));

            bundles.Add(new ScriptBundle("~/publicationjs").Include("~/Scripts/app/publication-controller.js"));

            bundles.Add(new ScriptBundle("~/chatjs").Include("~/Scripts/app/chat-controller.js"));

            // сборка стилей

            var libsStyles = new[]
            {
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap-datepicker3.css",
                "~/Content/bootstrap-fileinput/css/fileinput.css",
                "~/Content/bootstrap-tagsinput.css"
            };

            bundles.Add(new StyleBundle("~/appcss").Include(libsStyles));
        }
    }
}