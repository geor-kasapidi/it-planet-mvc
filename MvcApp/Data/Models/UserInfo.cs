﻿using System;
using System.Collections.Generic;

using Microsoft.AspNet.Identity.EntityFramework;

namespace MvcApp.Data.Models
{
    public enum UserSex
    {
        Male,

        Female,

        NotSpecified
    }

    public class UserInfo : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public UserSex Sex { get; set; }

        public string Image { get; set; }

        public string Info { get; set; }

        public virtual ICollection<UserInfo> Friends { get; set; }

        public virtual ICollection<UserInfo> Subscribers { get; set; }

        public virtual ICollection<PublicationInfo> Publications { get; set; }

        public virtual ICollection<CommentInfo> Comments { get; set; }

        public virtual ICollection<PublicationInfo> Rated { get; set; }

        public virtual ICollection<PublicationInfo> Favorites { get; set; }
    }
}