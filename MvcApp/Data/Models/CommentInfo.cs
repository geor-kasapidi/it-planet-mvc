﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcApp.Data.Models
{
    public class CommentInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public virtual UserInfo Author { get; set; }

        public virtual PublicationInfo Publication { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime CreationDate { get; set; }
    }
}