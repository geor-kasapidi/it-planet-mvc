﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcApp.Data.Models
{
    public class PublicationInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public virtual UserInfo Author { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required, MaxLength]
        public string Content { get; set; }

        [Required]
        public string KeyWords { get; set; }

        public DateTime CreationDate { get; set; }

        public int Views { get; set; }

        public virtual ICollection<CommentInfo> Comments { get; set; }

        public virtual ICollection<UserInfo> WhoRated { get; set; }

        public virtual ICollection<UserInfo> WhoAddedToFavorites { get; set; }
    }
}