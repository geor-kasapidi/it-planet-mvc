﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcApp.Data.Models
{
    public class ChatMessageInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public virtual UserInfo Sender { get; set; }

        public virtual UserInfo Reciever { get; set; }

        [Required]
        public string Text { get; set; }
    }
}