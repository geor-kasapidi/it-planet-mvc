﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

using MvcApp.Data.Models;
using MvcApp.Identity;

namespace MvcApp.Data
{
    public class RandomDataGenerator : DropCreateDatabaseAlways<AppDbContext>
    {
        public override void InitializeDatabase(AppDbContext context)
        {
            base.InitializeDatabase(context);

            var userManager = new AppUserManager(new UserStore<UserInfo>(context));

            UserInfo user1 = null;

            UserInfo user2 = null;

            for (var i = 1; i <= 100; i++)
            {
                var user = CreateUser(userManager, String.Format("user{0}@site.com", i), String.Format("!Pswrd{0}!", i));

                if (i == 1)
                {
                    user1 = user;
                }

                if (i == 2)
                {
                    user2 = user;
                }
            }

            var random = new Random();

            var webClient = new WebClient();

            for (var index = 1; index <= 100; index++)
            {
                var keywords = String.Join(",", Enumerable.Range(0, 5).Select(num => String.Format("Tag{0}{1}", index, num)));

                var randomDate = new DateTime(random.Next(1990, 2015), random.Next(1, 12), random.Next(1, 25), random.Next(0, 24), random.Next(0, 60), 0);

                var content = webClient.DownloadString("http://loripsum.net/api/10/short/headers");

                var publication = new PublicationInfo
                {
                    Author = index%2 == 0 ? user1 : user2,
                    Title = String.Format("Publication {0} short title", index),
                    Description = String.Format("Publication {0} preview text. Not so long, but very informative.", index),
                    KeyWords = keywords,
                    CreationDate = randomDate,
                    Content = content
                };

                context.Publications.Add(publication);

                Thread.Sleep(100);

                Debug.WriteLine(index);
            }

            Debug.WriteLine("Saving...");

            context.SaveChanges();
        }

        private static UserInfo CreateUser(AppUserManager userManager, string email, string password)
        {
            var user = new UserInfo
            {
                UserName = email,
                Email = email,
                EmailConfirmed = true,
                RegistrationDate = DateTime.Now,
                Sex = UserSex.NotSpecified
            };

            userManager.Create(user, password);

            return user;
        }
    }
}