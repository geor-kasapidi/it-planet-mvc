﻿using System;
using System.Data.Entity;

using Microsoft.AspNet.Identity.EntityFramework;

using MvcApp.Data.Models;

namespace MvcApp.Data
{
    public class AppDbContext : IdentityDbContext<UserInfo>
    {
        private const string DbName = "AppData";

        // в качестве хранилища используется SQL Server Comapact 4

        public AppDbContext() : base(String.Format("DataSource=|DataDirectory|{0}.sdf", DbName))
        {
            RequireUniqueEmail = true;
        }

        public DbSet<PublicationInfo> Publications { get; set; }

        public DbSet<CommentInfo> Comments { get; set; }

        public DbSet<ChatMessageInfo> ChatsMessages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // у пользователя есть список друзей, и список тех, кто добавил его к себе в друзья

            modelBuilder.Entity<UserInfo>().HasMany(v => v.Friends).WithMany(v => v.Subscribers).Map(v => v.ToTable("Friends"));

            // у публикации есть автор, есть список тех, кто проголосовал за нее, и список тех, кто добавил ее в избранное

            modelBuilder.Entity<PublicationInfo>().HasRequired(v => v.Author).WithMany(v => v.Publications).WillCascadeOnDelete(false);

            modelBuilder.Entity<PublicationInfo>().HasMany(v => v.WhoRated).WithMany(v => v.Rated).Map(v => v.ToTable("Rated"));

            modelBuilder.Entity<PublicationInfo>().HasMany(v => v.WhoAddedToFavorites).WithMany(v => v.Favorites).Map(v => v.ToTable("Favorites"));

            modelBuilder.Entity<PublicationInfo>().ToTable("Publications");

            // у комментария есть автор, и публикация, к которой он относится

            modelBuilder.Entity<CommentInfo>().HasRequired(v => v.Author).WithMany(v => v.Comments);

            modelBuilder.Entity<CommentInfo>().HasRequired(v => v.Publication).WithMany(v => v.Comments);

            modelBuilder.Entity<CommentInfo>().ToTable("Comments");

            // у сообщения в чате есть автор (sender), есть тот, кому адресовано сообщение (reciever)

            modelBuilder.Entity<ChatMessageInfo>().HasRequired(v => v.Sender).WithMany();

            modelBuilder.Entity<ChatMessageInfo>().HasRequired(v => v.Reciever).WithMany().WillCascadeOnDelete(false);

            modelBuilder.Entity<ChatMessageInfo>().ToTable("ChatsMessages");
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }
}