﻿using System;
using System.Web;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;

using MvcApp.Data;
using MvcApp.Data.Models;
using MvcApp.Identity;
using MvcApp.ViewModels.User;

namespace MvcApp.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        public AppDbContext DbContext
        {
            get { return Context.Request.GetHttpContext().GetOwinContext().Get<AppDbContext>(); }
        }

        public AppUserManager UserManager
        {
            get { return Context.Request.GetHttpContext().GetOwinContext().GetUserManager<AppUserManager>(); }
        } 

        public async void Send(string userId, string message)
        {
            var sender = await UserManager.FindByEmailAsync(Context.User.Identity.Name);

            var reciever = await UserManager.FindByEmailAsync(userId);

            if (sender == null || reciever == null)
            {
                return;
            }

            if (sender.Id == reciever.Id) // нельзя отправить самому себе сообщение
            {
                return;
            }

            var text = (message ?? String.Empty).Trim();

            if (String.IsNullOrEmpty(text))
            {
                return;
            }

            DbContext.ChatsMessages.Add(new ChatMessageInfo
            {
                Sender = sender,
                Reciever = reciever,
                Text = text
            });

            await DbContext.SaveChangesAsync();

            var viewModel = new UserChatMessageViewModel
            {
                AuthorId = Context.User.Identity.GetUserId(),
                AuthorName = Context.User.Identity.Name,
                Text = text
            };

            Clients.Caller.sendMessage(viewModel);

            Clients.User(userId).sendMessage(viewModel);
        }
    }
}