﻿using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;

using MvcApp.Data.Models;

namespace MvcApp.Identity
{
    public static class UserAuthExtensions
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this UserInfo user, AppUserManager manager)
        {
            return await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }
    }
}