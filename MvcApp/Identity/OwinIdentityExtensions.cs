﻿using System;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;

using MvcApp.Data.Models;

using Owin;

namespace MvcApp.Identity
{
    public static class OwinIdentityExtensions
    {
        public static void MapIdentity(this IAppBuilder app)
        {
            // во время запроса будут доступны автоматически созданные экземпляры классов для управлению пользователями и ролями

            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);

            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);

            app.CreatePerOwinContext<AppSignInManager>(AppSignInManager.Create);

            // настройка куков

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                AuthenticationMode = AuthenticationMode.Active,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<AppUserManager, UserInfo>(TimeSpan.FromMinutes(30), (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
        }
    }
}