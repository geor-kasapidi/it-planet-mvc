﻿using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

using MvcApp.Data.Models;

namespace MvcApp.Identity
{
    public class AppSignInManager : SignInManager<UserInfo, string>
    {
        public AppSignInManager(UserManager<UserInfo, string> userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager) {}

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(UserInfo user)
        {
            return user.GenerateUserIdentityAsync((AppUserManager) UserManager);
        }

        public static AppSignInManager Create(IdentityFactoryOptions<AppSignInManager> options, IOwinContext context)
        {
            return new AppSignInManager(context.GetUserManager<AppUserManager>(), context.Authentication);
        }
    }
}