﻿using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;

namespace MvcApp.Identity
{
    public class UserEmailService : IIdentityMessageService
    {
        private const string UserEmail = "blog.it.planet@gmail.com";

        private const string UserPassword = "yQ0L5Q8drIm";

        public Task SendAsync(IdentityMessage message)
        {
            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(UserEmail, UserPassword),
                EnableSsl = true
            };

            var email = new MailMessage(UserEmail, message.Destination)
            {
                Subject = message.Subject,
                SubjectEncoding = Encoding.UTF8,
                Body = message.Body,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true
            };

            return smtp.SendMailAsync(email);
        }
    }
}