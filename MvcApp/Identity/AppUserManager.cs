﻿using System;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

using MvcApp.Data;
using MvcApp.Data.Models;

namespace MvcApp.Identity
{
    public class AppUserManager : UserManager<UserInfo, string>
    {
        public AppUserManager(IUserStore<UserInfo, string> store) : base(store) {}

        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var manager = new AppUserManager(new UserStore<UserInfo>(context.Get<AppDbContext>()));

            // настройка уникальности почтового адреса у пользователя

            manager.UserValidator = new UserValidator<UserInfo>(manager)
            {
                RequireUniqueEmail = true
            };

            // настройки пароля - минимальная длина, необходимые символы и т.д.

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 7,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            // блокировка входа после нескольких неудачных попыток ввода пароля

            manager.UserLockoutEnabledByDefault = true;

            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(1);

            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // настройка почты

            manager.EmailService = new UserEmailService();

            // настройка генерации токенов для сброса пароля и подтверждения профиля

            if (options.DataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<UserInfo>(options.DataProtectionProvider.Create());
            }

            return manager;
        }
    }
}